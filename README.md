
# Slogans, descriptions, and overviews

Hackerspace: Unlock your creativity and innovate with technology!

Ballarat Hackerspace (also known as a makerspace or a fablab) is a community-operated workspace where people with common interests in technology, science, or digital arts can gather to work on projects, collaborate with like-minded individuals, and share resources and knowledge. We are equipped with various tools and equipment, such as 3D printers, laser cutters, CNC machines, and electronics workstations, that members can use to prototype and build their projects. We also host workshops, classes, and events related to technology and innovation to help members improve their skills and learn new ones.
6:36
Ballarat Hackerspace is a registered non-profit organisation, where members pay a monthly fee to cover the cost of rent, utilities, and equipment maintenance, and in return, they gain access to the workspace, tools, and resources. Hackerspaces promote a culture of collaboration, creativity, and learning, where members can share their knowledge and work together to solve complex problems. They also serve as a hub for innovation and entrepreneurship, providing a platform for individuals to turn their ideas into tangible projects and products.
6:36
Also for future search reference: Logo font is https://fonts.adobe.com/fonts/fira-sans
fonts.adobe.comfonts.adobe.com
Fira Sans from Mozilla
A sans serif typeface with 94 styles, available from Adobe Fonts for sync and web use. Adobe Fonts is the easiest way to bring great type into your workflow, wherever you are.